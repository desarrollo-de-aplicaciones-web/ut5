<?php

namespace Project;

use PDO;
use PDOException;

class Connection
{
    private $host;
    private $db;
    private $user;
    private $pass;
    private $dsn;
    protected $link;

    public function __construct()
    {
        $this->host = "localhost";
        $this->db = "project";
        $this->user = "manager";
        $this->pass = "secret";
        $this->dsn = "mysql:host={$this->host};dbname={$this->db};charset=utf8mb4";
        
        $this->init();
    }

    public function init()
    {
        try {
            $this->link = new PDO($this->dsn, $this->user, $this->pass);
            $this->link->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $ex) {
            die("Error en la conexión: mensaje: " . $ex->getMessage());
        }
        return $this->link;
    }
}
