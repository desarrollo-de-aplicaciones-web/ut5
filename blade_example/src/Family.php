<?php
namespace Project;

use PDO;
use PDOException;

class Family extends Connection {
        
    public function __construct()
    {
        parent::__construct();
    }

    public function getAllFamilies() {
        $stmt = $this->link->prepare("SELECT * FROM families ORDER BY code");
        try {
            $stmt->execute();
        } catch (PDOException $ex) {
            die("Error al recuperar: " . $ex->getMessage());
        }
        return $stmt->fetchAll(PDO::FETCH_OBJ);        
    }
}
