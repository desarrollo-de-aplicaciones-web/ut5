<?php
namespace Project;

use PDO;
use PDOException;

class Product extends Connection {

    public function __construct()
    {
        parent::__construct();
    }

    public function getAllProducts() {        
        $stmt = $this->link->prepare("SELECT * FROM products ORDER BY id");
        try {
            $stmt->execute();
        } catch (PDOException $ex) {
            die("Error al recuperar productos: " . $ex->getMessage());
        }        
        return $stmt->fetchAll(PDO::FETCH_OBJ);
    }
}