<?php $__env->startSection('title'); ?>
    <?php echo e($title); ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('header'); ?>
    <?php echo e($header); ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <table class="table table-striped table-hover">
        <thead>
        <tr class="text-center">
            <th scope="col">Código</th>
            <th scope="col">Nombre</th>
            <th scope="col">Nombre corto</th>
            <th scope="col">Precio</th>
        </tr>
        </thead>
        <tbody>
        <?php $__currentLoopData = $products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <tr class="text-center">
                <th scope="row"><?php echo e($item->id); ?></th>
                <td><?php echo e($item->name); ?></td>
                <td><?php echo e($item->short_name); ?></td>
                <?php if($item->price>100): ?>            
                    <td class='text-danger'><?php echo e(number_format($item->price, 2, ",", ".")); ?> &euro;</td>
                <?php else: ?>
                    <td class='text-success'><?php echo e(number_format($item->price, 2, ",", ".")); ?> &euro;</td>    
                <?php endif; ?>
            </tr>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </tbody>
    </table>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.base', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/vagrant/code/DSW/sandbox/public/ut5/composerExample/views/products.blade.php ENDPATH**/ ?>