@extends('layouts.base')
@section('title')
    {{$title}}
@endsection
@section('header')
    {{$header}}
@endsection

@section('content')
    <table class="table table-striped table-hover">
        <thead>
        <tr class="text-center">
            <th scope="col">Código</th>
            <th scope="col">Nombre</th>
        </tr>
        </thead>
        <tbody>
        @foreach($families as $item)
            <tr class="text-center">
                <th scope="row">{{$item->code}}</th>
                <td>{{$item->name}}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection
