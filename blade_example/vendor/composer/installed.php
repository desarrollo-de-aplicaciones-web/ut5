<?php return array (
  'root' => 
  array (
    'pretty_version' => '1.0.0+no-version-set',
    'version' => '1.0.0.0',
    'aliases' => 
    array (
    ),
    'reference' => NULL,
    'name' => 'dsw/project-example',
  ),
  'versions' => 
  array (
    'doctrine/inflector' => 
    array (
      'pretty_version' => '1.4.3',
      'version' => '1.4.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '4650c8b30c753a76bf44fb2ed00117d6f367490c',
    ),
    'dsw/project-example' => 
    array (
      'pretty_version' => '1.0.0+no-version-set',
      'version' => '1.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'illuminate/container' => 
    array (
      'pretty_version' => 'v5.8.36',
      'version' => '5.8.36.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b42e5ef939144b77f78130918da0ce2d9ee16574',
    ),
    'illuminate/contracts' => 
    array (
      'pretty_version' => 'v5.8.36',
      'version' => '5.8.36.0',
      'aliases' => 
      array (
      ),
      'reference' => '00fc6afee788fa07c311b0650ad276585f8aef96',
    ),
    'illuminate/events' => 
    array (
      'pretty_version' => 'v5.8.36',
      'version' => '5.8.36.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a85d7c273bc4e3357000c5fc4812374598515de3',
    ),
    'illuminate/filesystem' => 
    array (
      'pretty_version' => 'v5.8.36',
      'version' => '5.8.36.0',
      'aliases' => 
      array (
      ),
      'reference' => '494ba903402d64ec49c8d869ab61791db34b2288',
    ),
    'illuminate/support' => 
    array (
      'pretty_version' => 'v5.8.36',
      'version' => '5.8.36.0',
      'aliases' => 
      array (
      ),
      'reference' => 'df4af6a32908f1d89d74348624b57e3233eea247',
    ),
    'illuminate/view' => 
    array (
      'pretty_version' => 'v5.8.36',
      'version' => '5.8.36.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c859919bc3be97a3f114377d5d812f047b8ea90d',
    ),
    'nesbot/carbon' => 
    array (
      'pretty_version' => '2.45.1',
      'version' => '2.45.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '528783b188bdb853eb21239b1722831e0f000a8d',
    ),
    'philo/laravel-blade' => 
    array (
      'pretty_version' => 'v3.1',
      'version' => '3.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '3f0ce2ee198604c53c25188110e6d7b5e887527a',
    ),
    'psr/container' => 
    array (
      'pretty_version' => '1.0.0',
      'version' => '1.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b7ce3b176482dbbc1245ebf52b181af44c2cf55f',
    ),
    'psr/log' => 
    array (
      'pretty_version' => '1.1.3',
      'version' => '1.1.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '0f73288fd15629204f9d42b7055f72dacbe811fc',
    ),
    'psr/simple-cache' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '408d5eafb83c57f6365a3ca330ff23aa4a5fa39b',
    ),
    'symfony/debug' => 
    array (
      'pretty_version' => 'v4.4.19',
      'version' => '4.4.19.0',
      'aliases' => 
      array (
      ),
      'reference' => 'af4987aa4a5630e9615be9d9c3ed1b0f24ca449c',
    ),
    'symfony/finder' => 
    array (
      'pretty_version' => 'v4.4.19',
      'version' => '4.4.19.0',
      'aliases' => 
      array (
      ),
      'reference' => '25d79cfccfc12e84e7a63a248c3f0720fdd92db6',
    ),
    'symfony/polyfill-mbstring' => 
    array (
      'pretty_version' => 'v1.22.1',
      'version' => '1.22.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '5232de97ee3b75b0360528dae24e73db49566ab1',
    ),
    'symfony/polyfill-php80' => 
    array (
      'pretty_version' => 'v1.22.1',
      'version' => '1.22.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'dc3063ba22c2a1fd2f45ed856374d79114998f91',
    ),
    'symfony/translation' => 
    array (
      'pretty_version' => 'v5.2.3',
      'version' => '5.2.3.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c021864d4354ee55160ddcfd31dc477a1bc77949',
    ),
    'symfony/translation-contracts' => 
    array (
      'pretty_version' => 'v2.3.0',
      'version' => '2.3.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e2eaa60b558f26a4b0354e1bbb25636efaaad105',
    ),
    'symfony/translation-implementation' => 
    array (
      'provided' => 
      array (
        0 => '2.0',
      ),
    ),
  ),
);
