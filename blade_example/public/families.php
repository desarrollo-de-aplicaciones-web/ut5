<?php
require '../vendor/autoload.php';

use Philo\Blade\Blade;
use Project\Family;

$views = '../views';
$cache = '../cache';
$blade = new Blade($views, $cache);

$title = 'Familias';
$header = 'Listado de familias';
$families = (new Family())->getAllFamilies();

echo $blade->view()->make('families', compact('title', 'header', 'families'))->render();