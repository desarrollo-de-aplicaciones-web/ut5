<?php
require '../vendor/autoload.php';

use Philo\Blade\Blade;
use Project\Product;

$views = '../views';
$cache = '../cache';
$blade = new Blade($views, $cache);

$title = "Productos";
$header = "Listado de productos";
$products = (new Product())->getAllProducts();

echo $blade->view()->make('products', compact('title', 'header', 'products'))->render();